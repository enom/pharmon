#!/usr/bin/env php
<?php

// Attempt to load Composer from the usual directories
$source = __DIR__.'/../vendor/autoload.php';
$vendor = __DIR__.'/../../../autoload.php';

if (file_exists($source)) {
    require $source;
} else {
    require $vendor;
}

use Pharmon\Monitor;
use Pharmon\Console;

// -----------------------------------------------------------------------------
// PROCESSING ARGUMENTS
// -----------------------------------------------------------------------------

// Access command line arguments
$cli = new Console\CommandLine();

// Prioritize help message
if ($cli->arg('help') ?? $cli->arg('h')) {
    // Display the full help message
    Console\Help::printHelp();

    // Exit without error
    exit(0);
}

// Directory to watch
$dirs = $cli->arg('watch') ?? $cli->arg('w', getcwd());

// I/O events to listen to
$events = IN_CLOSE_WRITE | IN_CREATE | IN_MODIFY | IN_MOVED_TO;

// Commands to run
$exec = $cli->arg('exec') ?? $cli->arg('x');

// Extensions to watch(comma separated (e.g. php,js,css))
$ext = $cli->arg('extensions') ?? $cli->arg('ext') ?? $cli->arg('e');

// Convert relative to absolute paths
$paths = array_map(static function (string $item) {
    return '/' === $item[0] ? $item : getcwd().DIRECTORY_SEPARATOR.$item;
}, is_array($dirs) ? $dirs : [$dirs]);

// Skip initial execution of command
$skip = $cli->arg('skip-startup') ?? $cli->arg('s', false);

// There are several verbosity levels for printing messages, null being default
$verbose = null;

if ($cli->arg('debug') ?? $cli->arg('vv')) {
    // Prefer debug over verbose
    $verbose = Console\Prints::VERBOSE_DEBUG;
} elseif ($cli->arg('verbose') ?? $cli->arg('v')) {
    // Prints additional messages
    $verbose = Console\Prints::VERBOSE_VERBOSE;
}

// -----------------------------------------------------------------------------
// INVOKING WATCHER
// -----------------------------------------------------------------------------

try {
    // Creates inotify directory watchers for each folder/sub-folder found
    $watch = new Pharmon\Watcher($paths, $events, $verbose);

    // Registering our command runner as an observer
    $watch->attach(new Monitor\Observer($ext, $exec, $skip));

    // Passing nothing will forward the inotify read to our observer
    $watch->listen();
} catch (\Exception $e) {
    // Echo the error message instead of throwing it
    echo 'Could not start pharmon: '.$e->getMessage()."\n";

    // Exit with non-zero status means the process errored out
    exit(1);
}
