# Pharmon

Inspired by [nodemon](https://github.com/remy/nodemon). For use during
development of a PHP based application.

Pharmon will watch the files in the directory in which Pharmon was started, and
if any files change, Pharmon will automatically restart your PHP application.

**Warning** current release uses [`system()`](http://php.net/manual/en/function.system.php)
to execute commands.

[![Build](https://gitlab.com/enom/pharmon/badges/master/pipeline.svg)](https://gitlab.com/enom/pharmon/commits/master)
[![StyleCI](https://gitlab.styleci.io/repos/6522421/shield?branch=master)](https://gitlab.styleci.io/repos/6522421)

# Installation

Globally installing Pharmon.

```sh
composer global require enom/pharmon
```

You will need to add Composer's bin directory to your PATH in `~/.bash_profile`
(or `~/.bashrc`) by adding the following line:

```sh
export PATH=~/.composer/vendor/bin:$PATH
```

# Usage

Watches directories and runs commands on file changes.

```sh
pharmon
# Runs `php index.php` and watches the current directory
```

Useful for linting and testing. You can specify one or more directories to
watch by using the `-w` flag and one or more commands to execute with the
`-x` flag:

```sh
pharmon -w src/ -w tests/ -x 'composer run test'
# Runs `composer run test` and watches the specified directories
```

You can also get the absolute file path that triggered the change using `@` in
your command string:

```sh
pharmon -w src/ -x 'composer run lint:file @'
# Runs (e.g.) `composer run lint:file /var/www/pharmon/src/Pharmon/Watcher.php`
```

The `@` symbol can be escaped in your command string which will prevent the
replacement from hapening:

```sh
pharmon -w dist/ -e js,css -x 'scp @ deploy\@env.host:@'
# Runs (e.g.) `scp /var/www/dist/app.js deploy@env.host:/var/www/dist/app.js`
```

# Limitations

PHP doesn't come with multithreading support enabled by default so there's no
way to listen for both filesystem events and manual restarts(#9).

# Development

Please make sure `composer run standard` and `composer run test` are both
passing before pushing.
