<?php

/*
 * Copyright 2018 Jonathan Ginn <enom@enom.ws>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use Pharmon\Monitor;

/**
 * Class that handles testing inotify events.
 *
 * phpcs:disable PEAR.NamingConventions.ValidFunctionName.PublicUnderscore
 */
class WatcherTest extends \Codeception\Test\Unit
{
    /**
     * List of bit masks fired when appending to a file.
     *
     * @var int[]
     */
    public const APPEND = [IN_OPEN, IN_MODIFY, IN_CLOSE_WRITE];

    /**
     * List of bit masks fired when creating a new file.
     *
     * @var int[]
     */
    public const CREATE = [IN_CREATE, IN_OPEN, IN_MODIFY, IN_CLOSE_WRITE];

    /**
     * Bit mask for accessing a directory.
     *
     * @var int
     */
    public const DIR_ACCESS = IN_ACCESS | IN_ISDIR;

    /**
     * Bit mask for closing a directory.
     *
     * @var int
     */
    public const DIR_CLOSE = IN_CLOSE_NOWRITE | IN_ISDIR;

    /**
     * Bit mask for creating a directory.
     *
     * @var int
     */
    public const DIR_CREATE = IN_CREATE | IN_ISDIR;

    /**
     * Bit mask for deleting a directory.
     *
     * @var int
     */
    public const DIR_DELETE = IN_DELETE | IN_ISDIR;

    /**
     * Bit mask for opening a directory.
     *
     * @var int
     */
    public const DIR_OPEN = IN_OPEN | IN_ISDIR;

    /**
     * List of all events fired for listing a directory.
     *
     * @var int[]
     */
    public const LS = [self::DIR_OPEN, self::DIR_ACCESS, self::DIR_CLOSE];

    /**
     * List of bit masks for creating a directory.
     *
     * @var int[]
     */
    public const MKDIR = [self::DIR_CREATE];

    /**
     * List of bit masks for reading a file.
     *
     * @var int[]
     */
    public const READ = [IN_OPEN, IN_ACCESS, IN_CLOSE_NOWRITE];

    /**
     * List of bit masks fired when updating a file.
     *
     * @var int[]
     */
    public const REPLACE = [IN_ACCESS, IN_OPEN, IN_MODIFY, IN_CLOSE_WRITE];

    /**
     * List of bit masks fired when deleting a directory.
     *
     * @var int[]
     */
    public const RMDIR = [IN_DELETE_SELF, IN_IGNORED, self::DIR_DELETE];

    /**
     * List of bit masks fired when touching a directory.
     *
     * @var int[]
     */
    public const TOUCH = [IN_OPEN | IN_ISDIR, IN_CLOSE_NOWRITE | IN_ISDIR, IN_ATTRIB | IN_ISDIR];

    /**
     * Test directory path.
     *
     * @var array
     */
    public $paths = [
        '/tmp/pharmon-test-a',
        '/tmp/pharmon-test-b',
    ];

    /**
     * Cleans up after each test.
     */
    public function _after()
    {
        try {
            foreach ($this->paths as $path) {
                $this->rmdir($path);
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Sets up before each test.
     */
    public function _before()
    {
        try {
            mkdir($this->paths[0]);
        } catch (\Exception $e) {
        }

        try {
            mkdir($this->paths[1]);
        } catch (\Exception $e) {
        }
    }

    /**
     * Asserts an array of event masks is equal to the events passed.
     *
     * @param int[]                    $masks  Masks to validate
     * @param \Pharmon\Monitor\Event[] $events Events to read from
     */
    public function assertEventMasks(array $masks, array $events)
    {
        $this->assertEquals($masks, $this->masks($events));
    }

    /**
     * Returns the list of masks from a series of events.
     *
     * @param array $events Events
     *
     * @return array
     */
    public function masks(array $events)
    {
        return array_map(function (Monitor\Event $event) {
            return $event->mask;
        }, $events);
    }

    /**
     * Recursively deletes directories and files.
     *
     * @param string $dir Directory to delete
     */
    public function rmdir($dir)
    {
        $handler = opendir($dir);

        while (false !== ($file = readdir($handler))) {
            if (('.' != $file) && ('..' != $file)) {
                $path = $dir.'/'.$file;

                if (is_dir($path)) {
                    $this->rmdir($path);
                } else {
                    unlink($path);
                }
            }
        }

        closedir($handler);

        rmdir($dir);
    }

    /**
     * Tests creating and deleting directories.
     */
    public function testCreatingDirectories()
    {
        // Folder paths
        $base = $this->paths[0];
        $one = 'one';
        $onePath = $base.\DIRECTORY_SEPARATOR.$one;
        $bravo = 'bravo';
        $bravoPath = $onePath.\DIRECTORY_SEPARATOR.$bravo;

        // Watching /tmp/pharmon-test-a
        $watcher = new Pharmon\Watcher($base, IN_ALL_EVENTS);

        // Setting up initial watcher accesses the directory
        $this->assertEventMasks(self::LS, $watcher->read());

        // Starting with one watcher
        $this->assertCount(1, $watcher->directories());

        // mkdir /tmp/pharmon-test-a/one
        mkdir($onePath);

        // System listens for IN_CREATE events
        $this->assertEventMasks(self::MKDIR, $watcher->read());

        // We now have two watchers instead of one
        $this->assertCount(2, $watcher->directories());

        // mkdir /tmp/pharmon-test-a/one/bravo
        mkdir($bravoPath);

        // Another IN_CREATE event read
        $this->assertEventMasks(self::MKDIR, $watcher->read());

        // Now there's three watched directories
        $this->assertCount(3, $watcher->directories());

        // rmdir /tmp/pharmon-test-a/one/bravo
        rmdir($bravoPath);

        // Removing a directory
        $this->assertEventMasks(self::RMDIR, $watcher->read());

        // Back down to two watchers
        $this->assertCount(2, $watcher->directories());

        // mkdir /tmp/pharmon-test-a/one/bravo
        mkdir($bravoPath);

        // rm -rf /tmp/pharmon-test-a/one
        system('rm -rf '.$onePath);

        // Creating a child directory and deleting the parent directory
        $events = $watcher->read();
        $first = $events[0];
        $last = $events[count($events) - 1];

        // Despite the events saying created, the directory might have been deleted
        // prior to event processing
        $this->assertEquals([self::DIR_CREATE, self::DIR_DELETE], [$first->mask, $last->mask]);

        // Back down to one directory
        $this->assertCount(1, $watcher->directories());
    }

    /**
     * Tests invoking with invalid arguments.
     */
    public function testInvalidDirectory()
    {
        $dir = uniqid();
        $invalid = Pharmon\Watcher::class.' expects $base to be an array of valid directories';
        $nonexistent = Pharmon\Watcher::class.' expects "'.$dir.'" to be a valid directory';

        try {
            new Pharmon\Watcher(true, IN_ALL_EVENTS);
        } catch (\Exception $e) {
            $this->assertEquals($invalid, $e->getMessage());
        }

        try {
            new Pharmon\Watcher($dir, IN_ALL_EVENTS);
        } catch (\Exception $e) {
            $this->assertEquals($nonexistent, $e->getMessage());
        }
    }

    /**
     * Tests multiple directory event listening.
     */
    public function testMultipleDirectories()
    {
        $watcher = new Pharmon\Watcher($this->paths, IN_ALL_EVENTS);

        // Events
        $ls = array_merge(self::LS, self::LS);

        // Starts by listing both directories
        $this->assertEventMasks($ls, $watcher->read());

        // Update directory timestamp
        touch($this->paths[0]);

        // Touch updates directory metadata
        $this->assertEventMasks(self::TOUCH, $watcher->read());

        // Update the other directory timestamp
        touch($this->paths[1]);

        // Touch updates directory metadata
        $this->assertEventMasks(self::TOUCH, $watcher->read());
    }

    /**
     * Tests single directory event listening.
     */
    public function testSingleFile()
    {
        $base = $this->paths[0];
        $one = $base.\DIRECTORY_SEPARATOR.'one.txt';
        $watcher = new Pharmon\Watcher($base, IN_ALL_EVENTS);

        // Setting up initial watcher accesses the directory
        $this->assertEventMasks(self::LS, $watcher->read());

        // Starting with one watcher
        $this->assertCount(1, $watcher->directories());

        // Create file
        file_put_contents($one, 'created');

        // Events should reflect a single file created
        $this->assertEventMasks(self::CREATE, $watcher->read());

        // Replace file contents
        file_put_contents($one, 'replaced');

        // Replacing contents has four events
        $this->assertEventMasks(self::REPLACE, $watcher->read());

        // Read contents
        $contents = file_get_contents($one);

        // Check read events
        $this->assertEventMasks(self::READ, $watcher->read());

        // Check contents are same
        $this->assertEquals('replaced', $contents);

        // Append instead of replace
        file_put_contents($one, 'updated', \FILE_APPEND);

        // Appending contents has three events; it doesn't call IN_ACCESS
        $this->assertEventMasks(self::APPEND, $watcher->read());

        // Delete the file
        unlink($one);

        // Check delete events
        $this->assertEventMasks([IN_DELETE], $watcher->read());
    }
}
