<?php

/*
 * Copyright 2018 Jonathan Ginn <enom@enom.ws>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Pharmon\Monitor;

/**
 * Wrapper for inotify events.
 */
class Event
{
    /**
     * Inotify constant for file accessed.
     *
     * @var string
     */
    public const ACCESS = 'IN_ACCESS';

    /**
     * Inotify constant for all events.
     *
     * @var string
     */
    public const ALL_EVENTS = 'IN_ALL_EVENTS';

    /**
     * Inotify constant for metadata changed.
     *
     * @var string
     */
    public const ATTRIB = 'IN_ATTRIB';

    /**
     * Inotify constant for both IN_CLOSE_NOWRITE and IN_CLOSE_WRITE.
     *
     * @var string
     */
    public const CLOSE = 'IN_CLOSE';

    /**
     * Inotify constant for closing nowrite files.
     *
     * @var string
     */
    public const CLOSE_NOWRITE = 'IN_CLOSE_NOWRITE';

    /**
     * Inotify constant for closing written files.
     *
     * @var string
     */
    public const CLOSE_WRITE = 'IN_CLOSE_WRITE';

    /**
     * Inotify constant for creating a directory or file.
     *
     * @var string
     */
    public const CREATE = 'IN_CREATE';

    /**
     * Inotify constant for deleting a directory or file.
     *
     * @var string
     */
    public const DELETE = 'IN_DELETE';

    /**
     * Inotify constant for deleting watched directory.
     *
     * @var string
     */
    public const DELETE_SELF = 'IN_DELETE_SELF';

    /**
     * Inotify constant for following symlinks.
     *
     * @var string
     */
    public const DONT_FOLLOW = 'IN_DONT_FOLLOW';

    /**
     * Inotify constant for inotify_rm_watch() event.
     *
     * @var string
     */
    public const IGNORED = 'IN_IGNORED';

    /**
     * Inotify constant for path being a directory.
     *
     * @var string
     */
    public const ISDIR = 'IN_ISDIR';

    /**
     * Inotify constant for adding events to watch mask.
     *
     * @var string
     */
    public const MASK_ADD = 'IN_MASK_ADD';

    /**
     * Inotify constant for file being modified.
     *
     * @var string
     */
    public const MODIFY = 'IN_MODIFY';

    /**
     * Inotify constant for IN_MOVED_FROM and IN_MOVED_TO.
     *
     * @var string
     */
    public const MOVE = 'IN_MOVE';

    /**
     * Inotify constant for moving files out of a watched directory.
     *
     * @var string
     */
    public const MOVED_FROM = 'IN_MOVED_FROM';

    /**
     * Inotify constant for moving files into a watched directory.
     *
     * @var string
     */
    public const MOVED_TO = 'IN_MOVED_TO';

    /**
     * Inotify constant for watch directory moving.
     *
     * @var string
     */
    public const MOVE_SELF = 'IN_MOVE_SELF';

    /**
     * Inotify constant for getting only one event.
     *
     * @var string
     */
    public const ONESHOT = 'IN_ONESHOT';

    /**
     * Inotify constant for only watching directories.
     *
     * @var string
     */
    public const ONLYDIR = 'IN_ONLYDIR';

    /**
     * Inotify constant for file being opened.
     *
     * @var string
     */
    public const OPEN = 'IN_OPEN';

    /**
     * Inotify constant for even queue overflow.
     *
     * @var string
     */
    public const Q_OVERFLOW = 'IN_Q_OVERFLOW';

    /**
     * Inotify constant for file system unmount.
     *
     * @var string
     */
    public const UNMOUNT = 'IN_UNMOUNT';

    /**
     * Description of the mask event.
     *
     * @var string
     */
    public $description;

    /**
     * The name of the file triggering the event.
     *
     * @var string
     */
    public $file;

    /**
     * Externally set path for the file triggering the event.
     *
     * @var string
     */
    public $path;

    /**
     * The mask of the event triggered.
     *
     * @var int
     */
    public $mask;

    /**
     * Raw event data received from inotify_read.
     *
     * @var array
     */
    public $raw;

    /**
     * Unique (inotify instance wide) watch descriptor.
     *
     * @see http://php.net/manual/en/function.inotify-add-watch.php
     *
     * @var int
     */
    public $watcher;

    /**
     * Load inotify event data.
     *
     * @param array $event inotify event data
     */
    public function __construct($event)
    {
        // Keep an instance of the raw event
        $this->raw = $event;

        // Proxy accessors
        $this->file = $event['name'];
        $this->mask = $event['mask'];
        $this->watcher = $event['wd'];

        $descriptions = $this->descriptions();

        // Remove the unnecessary message about directory events
        unset($descriptions[self::ISDIR]);

        // Setting description to first descriptor
        $this->description = array_values($descriptions)[0];
    }

    /**
     * Returns the list of mask descriptions that apply to this event.
     *
     * @return array
     */
    public function descriptions()
    {
        $descriptions = self::listDescriptions();
        $masks = [];

        foreach (self::listMasks() as $const => $mask) {
            if ($this->is($mask)) {
                $masks[$const] = $descriptions[$const];
            }
        }

        return $masks;
    }

    /**
     * Returns the full path to the file triggering the event.
     *
     * Note that `$this->path` must be set manually. inotify only returns the
     * filename, not the full path. Directory paths are not terminated by a
     * separator.
     *
     * @return string
     */
    public function fullpath()
    {
        return rtrim($this->path, \DIRECTORY_SEPARATOR).\DIRECTORY_SEPARATOR.$this->file;
    }

    /**
     * Checks the event bit mask against the provided $mask.
     *
     * @param int $mask Bit to match
     *
     * @return bool
     */
    public function is($mask)
    {
        return ($this->mask & $mask) === $mask;
    }

    /**
     * Checks if the event bit mask is that of a directory.
     *
     * @return bool
     */
    public function isDirectory()
    {
        return $this->is(IN_ISDIR);
    }

    /**
     * Returns the list of mask and their constants.
     *
     * @return array
     */
    public static function listMasks()
    {
        return [
           self::ACCESS => IN_ACCESS,
           self::ALL_EVENTS => IN_ALL_EVENTS,
           self::ATTRIB => IN_ATTRIB,
           self::CLOSE => IN_CLOSE,
           self::CLOSE_NOWRITE => IN_CLOSE_NOWRITE,
           self::CLOSE_WRITE => IN_CLOSE_WRITE,
           self::CREATE => IN_CREATE,
           self::DELETE => IN_DELETE,
           self::DELETE_SELF => IN_DELETE_SELF,
           self::DONT_FOLLOW => IN_DONT_FOLLOW,
           self::IGNORED => IN_IGNORED,
           self::ISDIR => IN_ISDIR,
           self::MASK_ADD => IN_MASK_ADD,
           self::MODIFY => IN_MODIFY,
           self::MOVE => IN_MOVE,
           self::MOVED_FROM => IN_MOVED_FROM,
           self::MOVED_TO => IN_MOVED_TO,
           self::MOVE_SELF => IN_MOVE_SELF,
           self::ONESHOT => IN_ONESHOT,
           self::ONLYDIR => IN_ONLYDIR,
           self::OPEN => IN_OPEN,
           self::Q_OVERFLOW => IN_Q_OVERFLOW,
           self::UNMOUNT => IN_UNMOUNT,
        ];
    }

    /**
     * Returns the list of masks and their description.
     *
     * The events marked with an asterisk (*) above can occur for files in
     * watched directories.
     *
     * @return array
     *
     * @see https://www.php.net/manual/en/inotify.constants.php
     */
    public static function listDescriptions()
    {
        return [
            self::ACCESS => 'File was accessed (read) (*)',
            self::ALL_EVENTS => 'Bitmask of all the above constants',
            self::ATTRIB => 'Metadata changed (e.g. permissions, mtime, etc.) (*)',
            self::CLOSE => 'Equals to IN_CLOSE_WRITE | IN_CLOSE_NOWRITE',
            self::CLOSE_NOWRITE => 'File not opened for writing was closed (*)',
            self::CLOSE_WRITE => 'File opened for writing was closed (*)',
            self::CREATE => 'File or directory created in watched directory (*)',
            self::DELETE => 'File or directory deleted in watched directory (*)',
            self::DELETE_SELF => 'Watched file or directory was deleted',
            self::DONT_FOLLOW => 'Do not dereference pathname if it is a symlink (Since Linux 2.6.15)',
            self::IGNORED => 'Watch was removed (explicitly by inotify_rm_watch() or because file was removed or filesystem unmounted)',
            self::ISDIR => 'Subject of this event is a directory',
            self::MASK_ADD => 'Add events to watch mask for this pathname if it already exists (instead of replacing mask).',
            self::MODIFY => 'File was modified (*)',
            self::MOVE => 'Equals to IN_MOVED_FROM | IN_MOVED_TO',
            self::MOVED_FROM => 'File moved out of watched directory (*)',
            self::MOVED_TO => 'File moved into watched directory (*)',
            self::MOVE_SELF => 'Watch file or directory was moved',
            self::ONESHOT => 'Monitor pathname for one event, then remove from watch list.',
            self::ONLYDIR => 'Only watch pathname if it is a directory (Since Linux 2.6.15)',
            self::OPEN => 'File was opened (*)',
            self::Q_OVERFLOW => 'Event queue overflowed (wd is -1 for this event)',
            self::UNMOUNT => 'File system containing watched object was unmounted',
        ];
    }

    /**
     * Returns the list of masks that apply to this event.
     *
     * @return array
     */
    public function masks()
    {
        $masks = [];

        foreach (self::listMasks() as $const => $mask) {
            if ($this->is($mask)) {
                $masks[$const] = $mask;
            }
        }

        return $masks;
    }
}
