<?php

/*
 * Copyright 2018 Jonathan Ginn <enom@enom.ws>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Pharmon\Monitor;

use Pharmon;
use Pharmon\Console;
use SplObserver;
use SplSubject;

/**
 * Observer implementation for running console commands.
 *
 * @see https://www.php.net/manual/en/class.splobserver.php
 */
class Observer implements SplObserver
{
    /**
     * Command to execute.
     *
     * @var array
     */
    private $_commands = ['php index.php'];

    /**
     * Extensions to watch.
     *
     * @var array
     */
    private $_extensions = ['php'];

    /**
     * Observer constructor.
     *
     * @param string|array $ext     File extension(s) to watch
     * @param string|array $command Command(s) to execute
     * @param bool         $skip    Skip command call on start
     */
    public function __construct($ext = null, $command = null, bool $skip = false)
    {
        if (null !== $ext) {
            $this->_extensions = \is_array($ext) ? $ext : explode(',', $ext);
        }

        if (null !== $command) {
            $this->_commands = \is_array($command) ? $command : [$command];
        }

        // Pretty print parsed arrays
        $extensions = implode(', ', $this->_extensions);
        $scripts = implode('`, `', $this->_commands);

        Console\Prints::v('Extension(s) to look for: '.$extensions, 'observer');
        Console\Prints::v('Script(s) to execute: `'.$scripts.'`', 'observer');

        // Optionally execute all of our scripts on start up
        if (false === $skip) {
            $this->call();
        } else {
            Console\Prints::v('Skipping initial call to script(s)', 'observer');
        }
    }

    /**
     * Calls the command(s).
     *
     * @param string $path Replacement for file reference variable
     *
     * @todo Move exec from system call to a Composer\Command\RunScriptCommand
     */
    public function call(string $path = '')
    {
        $placeholder = Pharmon\Watcher::PATH_PLACEHOLDER;
        $escaped = '\\'.$placeholder;
        $regex = '/([^\\\])'.$placeholder.'/';

        foreach ($this->_commands as $command) {
            // Update command with file path if applicable(don't match \@)
            $replaced = preg_replace($regex, '$1'.$path, $command);

            // Replace the "\@" with the desired escaped "@"
            $call = str_replace($escaped, $placeholder, $replaced);

            Console\Prints::l('Executing script: '.$call);

            system($call);
        }
    }

    /**
     * Receive update from subject.
     *
     * @param SplSubject|\Pharmon\Monitor\Subject $subject Updated subject
     */
    public function update(SplSubject $subject)
    {
        $event = $subject->event;
        $path = $event->fullpath();
        $info = pathinfo($path);
        $ext = $info['extension'] ?? null;

        // Asterisk means all files
        $globAll = \in_array('*', $this->_extensions);
        $globExt = \in_array($ext, $this->_extensions);

        // Main event message to display
        $summary = 'Event summary: '.$event->description.' '.$path;

        if ($event->isDirectory()) {
            // Display debug message about skipping directory events
            Console\Prints::vv($summary, 'observer');
            Console\Prints::vv('Ignoring directory event', 'observer');
        } elseif ($globAll || $globExt) {
            // File event matching our extensions list
            Console\Prints::v($summary, 'observer');
            Console\Prints::l('Restarting due to changes');

            // Execute all of the scripts with the file path
            $this->call($path);
        } else {
            // File event not matching our extensions
            Console\Prints::vv($summary, 'observer');
            Console\Prints::vv('Ignoring '.($ext ?? 'empty').' file type', 'observer');
        }
    }
}
