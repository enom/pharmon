<?php

/*
 * Copyright 2018 Jonathan Ginn <enom@enom.ws>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Pharmon\Monitor;

use Pharmon\Console;

/**
 * Wrapper for inotify functionality.
 *
 * @see http://php.net/manual/en/book.inotify.php
 */
class Notify
{
    /**
     * Our inotify_init instance.
     *
     * @var resource
     */
    protected $inotify;

    /**
     * Bitmask of events to listen to.
     *
     * @var int
     */
    protected $flags;

    /**
     * Contains all inotify watchers indexed by their directory path.
     *
     * @var array
     */
    protected $watchers = [];

    /**
     * System events to manage directories.
     *
     * @var int[]
     */
    protected static $system = [
        IN_CREATE,          // mkdir
        IN_DELETE,          // rmdir
        IN_DELETE_SELF,     // rmdir
        IN_IGNORED,         // rmdir
        IN_MOVE,            // mv
        IN_MOVE_SELF,       // mv
    ];

    /**
     * Boots our inotify with the flags for event listening.
     *
     * @param int $flags Bitmask
     */
    public function __construct(int $flags)
    {
        $this->inotify = inotify_init();
        $this->flags = $this->injectFlags($flags);
    }

    /**
     * Adds a directory to our watchers.
     *
     * @param string $directory Directory path
     */
    public function add(string $directory)
    {
        if (!\array_key_exists($directory, $this->watchers)) {
            Console\Prints::vv('inotify_add_watch: '.$directory, 'notify');

            $this->watchers[$directory] = inotify_add_watch($this->inotify, $directory, $this->flags);
        }
    }

    /**
     * Converts an inotify_read event into a Pharmon\Monitor\Event.
     *
     * @param array $event Event from inotify_read
     *
     * @return \Pharmon\Monitor\Event
     */
    public function decorate(array $event)
    {
        return new Event($event);
    }

    /**
     * Returns the list of watched directories.
     *
     * @return array
     */
    public function directories()
    {
        return array_keys($this->watchers);
    }

    /**
     * Returns the path of an inotify_add_watch descriptor or null if it doesn't
     * exist.
     *
     * @param int $reference Watch descriptor
     *
     * @return string|null
     */
    public function directory(int $reference)
    {
        return array_flip($this->watchers)[$reference] ?? null;
    }

    /**
     * Update the given bitmask with our system flags.
     *
     * @param int $flags Bitmask
     *
     * @return int
     */
    protected function injectFlags(int $flags)
    {
        // Keeps the list of applicable event constants(e.g. IN_ACCESS)
        $apply = [];
        $inject = [];

        // Extrapolate flags applied
        foreach (Event::listMasks() as $const => $mask) {
            $flagged = ($flags & $mask) === $mask;
            $system = \in_array($mask, self::$system);

            if ($flagged) {
                // Display constant(e.g. IN_ACCESS) instead of message
                $apply[] = $const;
            } elseif ($system) {
                // Need to add our system events to listeners
                $flags = $flags | $mask;

                // Display constant instead of message here too
                $inject[] = $const;
            }
        }

        // Display all the events the watcher and our system registered
        $applied = implode(', ', $apply);
        $injected = implode(', ', $inject);

        Console\Prints::v('Listening for events: '.$applied, 'notify');

        if (!empty($inject)) {
            Console\Prints::vv('Listening for additional system events: '.$injected, 'notify');
        }

        return $flags;
    }

    /**
     * Reads the next inotify event.
     *
     * @param int $index Event index to read
     *
     * @return \Pharmon\Monitor\Event|\Pharmon\Monitor\Event[]
     */
    public function read(int $index = null)
    {
        $events = inotify_read($this->inotify);

        return null === $index
            ? array_map([$this, 'decorate'], $events)
            : $this->decorate($events[$index]);
    }

    /**
     * Removes an inotify from our watchers.
     *
     * References can be removed by their directory paths or the
     * inotify_add_watch descriptor.
     *
     * @param string|int $reference Directory or watch descriptor
     */
    public function remove($reference)
    {
        if (\is_int($reference) && \in_array($reference, $this->watchers)) {
            // Integers match watchers directly
            $this->removeWatcher($reference);
        }

        if (\is_string($reference) && \array_key_exists($reference, $this->watchers)) {
            // Matching path for path
            $this->removeWatcher($this->watchers[$reference]);
        }
    }

    /**
     * Removes a directory from our watchers by watcher.
     *
     * @param int $watcher Reference inotify watcher
     */
    protected function removeWatcher(int $watcher)
    {
        // Get directory by watcher
        $directory = array_flip($this->watchers)[$watcher];

        // Remove our local reference first
        unset($this->watchers[$directory]);

        Console\Prints::vv('@inotify_rm_watch: '.$directory, 'notify');

        // Removing a watcher that doesn't exist will throw a suppressible error
        @inotify_rm_watch($this->inotify, $watcher);
    }

    /**
     * Updates a watcher's directory reference or delete it if outside scope.
     *
     * @param int    $reference Watch descriptor
     * @param string $path      Updated path
     */
    public function update(int $reference, string $path)
    {
        // Fetch old path
        $old = array_flip($this->watchers)[$reference];

        // Remove old path reference
        unset($this->watchers[$old]);

        // Add new path reference
        $this->watchers[$path] = $reference;
    }
}
