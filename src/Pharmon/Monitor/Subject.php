<?php

/*
 * Copyright 2018 Jonathan Ginn <enom@enom.ws>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Pharmon\Monitor;

use Pharmon\Monitor;
use SplObserver;
use SplSubject;

/**
 * Basic implementation of the observer pattern for inotify event handling.
 *
 * @see https://www.php.net/manual/en/class.splsubject.php
 */
class Subject implements SplSubject
{
    /**
     * Wrapper for inotify events.
     *
     * @var \Pharmon\Monitor\Event
     */
    public $event;

    /**
     * Observers to notify.
     *
     * @var \SplObserver[]
     */
    protected $observers = [];

    /**
     * Attaches an observer.
     *
     * @param \SplObserver $observer Observer to attach
     */
    public function attach(SplObserver $observer)
    {
        $this->observers[spl_object_hash($observer)] = $observer;
    }

    /**
     * Detaches an observer.
     *
     * @param \SplObserver $observer Observer to detach
     */
    public function detach(SplObserver $observer)
    {
        unset($this->observers[spl_object_hash($observer)]);
    }

    /**
     * Notify all observers.
     */
    public function notify()
    {
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }

    /**
     * Sets the event property and notifies observers of the change.
     *
     * @param \Pharmon\Monitor\Event $event Event to publish
     */
    public function publish(Monitor\Event $event)
    {
        $this->event = $event;
        $this->notify();
    }
}
