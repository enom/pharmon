<?php

/*
 * Copyright 2018 Jonathan Ginn <enom@enom.ws>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Pharmon\Util;

/**
 * Simple collector.
 */
class Collector implements CollectorInterface
{
    /**
     * Item list.
     *
     * @var array
     */
    protected $items = [];

    /**
     * Adds an item to our list.
     *
     * @param mixed $item Anything you want
     */
    public function add($item)
    {
        $this->items[] = $item;
    }

    /**
     * Returns all items.
     *
     * @return array
     */
    public function all()
    {
        return $this->items;
    }
}
