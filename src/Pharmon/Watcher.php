<?php

/*
 * Copyright 2018 Jonathan Ginn <enom@enom.ws>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Pharmon;

use SplObserver;

/**
 * File system watcher.
 */
class Watcher
{
    /**
     * Placeholder string that will get replaced with the full file path of the
     * update event.
     *
     * @var string
     */
    public const PATH_PLACEHOLDER = '@';

    /**
     * Pharmon version.
     *
     * @var string
     */
    public const VERSION = '1.0.1';

    /**
     * List of base directories to watch.
     *
     * @var array
     */
    public $base;

    /**
     * Array of directories to skip adding.
     *
     * @var array
     */
    protected static $exclude = ['.', '..', '.git', 'node_modules', 'vendor'];

    /**
     * Helper inotify_init class.
     *
     * @var \Pharmon\Monitor\Notify
     */
    protected $inotify;

    /**
     * Watcher currently running/blocking.
     *
     * @var bool
     */
    protected $running = false;

    /**
     * Observer pattern subject for reading inofity events.
     *
     * @var \Pharmon\Monitor\Subject
     */
    protected $subject;

    /**
     * Watches all directories(recursively) in `$base` for inotify bitmask
     * `$events`.
     *
     * @param array|string $base    Base directories to watch
     * @param int          $events  Bitmask events
     * @param int          $verbose Print event messages
     *
     * @throws \Exception Invalid $base directory
     */
    public function __construct($base, int $events, int $verbose = null)
    {
        // Verbose prints additional event information
        if (null !== $verbose) {
            Console\Prints::$verbose = $verbose;
        }

        // Allows passing a string to watch a single directory
        if (\is_string($base)) {
            $base = [$base];
        }

        // Validate type
        if (!\is_array($base)) {
            throw new \Exception(__CLASS__.' expects $base to be an array of valid directories');
        }

        // Validate directories
        foreach ($base as $dir) {
            if (!is_dir($dir)) {
                throw new \Exception(__CLASS__.' expects "'.$dir.'" to be a valid directory');
            }
        }

        Console\Prints::l(self::VERSION);

        // Convert any relative base paths to absolute ones
        $real = array_map('realpath', $base);

        // Store our base paths
        $this->base = $real;
        $this->inotify = new Monitor\Notify($events);
        $this->subject = new Monitor\Subject();

        foreach ($real as $dir) {
            $this->inotify->add($dir);

            foreach ($this->recurse($dir) as $directory) {
                $this->inotify->add($directory);
            }
        }
    }

    /**
     * Adds an inotify watcher.
     *
     * @param \Pharmon\Monitor\Event $event Event triggering the addition
     */
    protected function addWatcher(Monitor\Event $event)
    {
        $this->inotify->add($event->fullpath());
    }

    /**
     * Register an observer.
     *
     * @param \SplObserver $observer observer to register
     */
    public function attach(SplObserver $observer)
    {
        $this->subject->attach($observer);
    }

    /**
     * Process all pending inotify events.
     *
     * Passing a `$callback` will invoke that function instead of calling
     * {@see \Pharmon\Monitor\Subject::notify()}.
     *
     * @param callable|null $callback Function to invoke
     */
    public function batch(callable $callback = null)
    {
        $events = $this->read();

        foreach ($events as $event) {
            if (null === $callback) {
                // Notify all observers of the event
                $this->subject->publish($event);
            } else {
                // A callable can be invoked like a normal function
                $callback($event);
            }
        }
    }

    /**
     * Returns the list of watched directories.
     *
     * @return array
     */
    public function directories()
    {
        return $this->inotify->directories();
    }

    /**
     * Event loop - blocking! Calls the user function whenever an event they
     * subscribed to is raised.
     *
     * Passing null as callback will trigger registered observers' notify
     * function instead.
     *
     * @param callable|null $callback Function to invoke
     * @param bool          $batch    Use batch() instead of next()
     */
    public function listen(callable $callback = null, bool $batch = false)
    {
        $base = \count($this->base);

        // Initial sub directory count
        $sub = \count($this->directories()) - $base;

        Console\Prints::l('Watching '.$base.' file(s)/folder(s)');
        Console\Prints::v('Watching '.$sub.' initial sub directories', 'watcher');

        // Allows us to stop our listener any time we receive an event
        $this->running = true;

        // Start our blocking event loop
        while ($this->running) {
            // Count watched directories and sub directories
            $count = \count($this->directories());

            // Subtract watched directories for new sub directory count
            $current = $count - $base;

            // Handle displaying updated sub directory count in debug mode
            if ($current !== $sub) {
                // Update sub directory number for next iteration
                $sub = $current;

                // Display debug message
                Console\Prints::vv('Watching '.$sub.' updated sub directories', 'watcher');
            }

            // Handle event dispatching
            if ($batch) {
                $this->batch($callback);
            } else {
                $this->next($callback);
            }

            // Make sure we still have directories to watch
            if (0 === $count) {
                Console\Prints::l('All watched directories removed; stopping script');

                // Stops our loop
                $this->running = false;
            }
        }
    }

    /**
     * Process the next inotify event.
     *
     * @param callable|null $callback Function to invoke
     */
    public function next(callable $callback = null)
    {
        $events = $this->read();

        // Get the first event
        $event = reset($events);

        if (null === $callback) {
            // Notify all observers of the event
            $this->subject->publish($event);
        } else {
            // A callable can be invoked like a normal function
            $callback($event);
        }
    }

    /**
     * Reads and returns all of the pending inotify messages.
     *
     * @return \Pharmon\Monitor\Event[]
     */
    public function read()
    {
        $events = $this->inotify->read();
        $count = \count($events);

        for ($i = 0; $i < $count; ++$i) {
            $event = $events[$i];
            $dir = $event->isDirectory();

            // Update full path
            $event->path = $this->inotify->directory($event->watcher);

            // Fetch resolved path
            $path = $event->fullpath();

            // Handle adding newly created directories to watchers
            if ($event->is(IN_CREATE) && is_dir($path)) {
                $this->addWatcher($event);
            }

            // Handle deleting directories
            if ($dir && ($event->is(IN_DELETE) || $event->is(IN_DELETE_SELF))) {
                $this->removeWatcher($event);
            }

            // Handle IN_IGNORED which can kind of be a IN_DELETED | IN_ISDIR
            if ($event->is(IN_IGNORED)) {
                $this->removeWatcher($event);
            }

            // Handle moving directories
            if ($dir && ($event->is(IN_MOVE) || $event->is(IN_MOVE_SELF))) {
                $this->updateWatcher($event);
            }

            $masks = implode(', ', array_keys($event->masks()));
            $message = 'Event '.($i + 1).'/'.$count.' read: '.$masks.' '.$path;

            // Debug verbosity prints all event messages
            Console\Prints::vv($message, 'watcher');
        }

        return $events;
    }

    /**
     * Traverses a directory tree and returns all of the directory paths.
     *
     * @param string                           $directory Folder to recurse
     * @param \Pharmon\Util\CollectorInterface $results   Results collector
     *
     * @return array
     */
    public function recurse(string $directory, Util\CollectorInterface $results = null)
    {
        // Verbose prints directories watched
        Console\Prints::vv('Recurse folder: '.$directory, 'watcher');

        if (null === $results) {
            $results = new Util\Collector();
        }

        // Recurse any sub folders found in directory
        foreach ($this->listDirectories($directory) as $path) {
            // Add to our collected results
            $results->add($path);

            // Recurse sub folders as well
            $this->recurse($path, $results);
        }

        // Returns the list of all sub directories
        return $results->all();
    }

    /**
     * Recurse a directory and returns all of the sub directories.
     *
     * @param string $directory Directory to recurse
     *
     * @return array
     */
    protected function listDirectories(string $directory)
    {
        $results = [];

        if (false !== ($dh = opendir($directory))) {
            while (false !== ($name = readdir($dh))) {
                $path = rtrim($directory, \DIRECTORY_SEPARATOR).\DIRECTORY_SEPARATOR.$name;

                if (is_dir($path) && !\in_array($name, self::$exclude)) {
                    // Appending directory separator to directory path
                    $results[] = $path.\DIRECTORY_SEPARATOR;
                }
            }

            closedir($dh);
        }

        return $results;
    }

    /**
     * Removes an inotify watcher.
     *
     * @param \Pharmon\Monitor\Event $event Event triggering the removal
     */
    protected function removeWatcher(Monitor\Event $event)
    {
        $this->inotify->remove($event->fullpath());
    }

    /**
     * Updates an inotify watcher path or removes it if moved outside the base
     * directories watched.
     *
     * @param Monitor\Event $event Event triggering the update
     */
    protected function updateWatcher(Monitor\Event $event)
    {
        $update = false;
        $path = $event->fullpath();

        foreach ($this->base as $base) {
            $update = $update || false !== strpos($path, $base);
        }

        if ($update) {
            // Only update if the new path is in our base directories
            $this->inotify->update($event->watcher, $path);
        } else {
            // Otherwise, delete the inotify
            $this->inotify->remove($path);
        }
    }
}
