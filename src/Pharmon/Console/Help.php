<?php

/*
 * Copyright 2018 Jonathan Ginn <enom@enom.ws>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Pharmon\Console;

/**
 * Console help messages helper.
 */
class Help extends Prints
{
    /**
     * Returns the list of examples for the help message.
     *
     * @return array
     */
    public static function getExamples()
    {
        return [
            'pharmon -w ../lib -x "php server.php"',
            'pharmon --watch tests/ --exec "composer run test"',
            'pharmon -e php,phtml -w views/ -x "composer run compile:flush" -x'.
                ' "composer run compile:views"',
        ];
    }

    /**
     * Returns the note part of the help message.
     *
     * @return string
     */
    public static function getNote()
    {
        return 'It\'s possible to watch multiple files/folders by specifying'.
            ' multiple -w(--watch) arguments. Similarly, execute multiple commands'.
            ' by specifying multiple -x(--exec) arguments which will run sequentially'.
            ' by order specified. By default, pharmon will monitor .php files and'.
            ' run "php index.php" as the command.';
    }

    /**
     * Returns the list of options for the help message.
     *
     * @return array
     */
    public static function getOptions()
    {
        return [
            '-e, --ext type' => 'Extensions to look for (e.g. -e php,phtml)',
            '-s, --skip-startup' => 'Skip executing the script on start up',
            '-v, --verbose' => 'Display additional log messages',
            '-vv, --debug' => 'Display debug messages',
            '-w, --watch path' => 'Watch directory or file "path" (use once for'.
                ' each path to watch)',
            '-x, --exec app' => 'Execute script "app" (e.g. -x "python -v")',
        ];
    }

    /**
     * Returns the usage part of the help message.
     *
     * @return string
     */
    public static function getUsage()
    {
        return 'Usage: pharmon [options]';
    }

    /**
     * Prints the examples help message.
     */
    public static function printExamples()
    {
        self::p('Examples:'."\n");

        foreach (self::getExamples() as $example) {
            self::p('   $ '.$example);
        }
    }

    /**
     * Prints the -h/--help message.
     */
    public static function printHelp()
    {
        self::printUsage();
        self::p();
        self::printOptions();
        self::p();
        self::printNote();
        self::p();
        self::printExamples();
    }

    /**
     * Prints the note help message.
     */
    public static function printNote()
    {
        self::p('Notes:'."\n");
        self::hardWrap(self::getNote(), '    ', '   ');
    }

    /**
     * Prints a given option help message.
     *
     * @param string $option      Option to print
     * @param string $description Description to print
     */
    public static function printOption(string $option, string $description)
    {
        // Vertical lineup of option descriptions by padding with dots
        $dots = str_repeat('.', 20 - \strlen($option));

        // Option text "   -w, --watch "path" .... "
        $prefix = '   '.$option.' '.$dots;

        // Full option text "   -w, --watch "path" .... Watch directory or..."
        $text = $prefix.' '.$description;

        if (\strlen($text) > self::MAX_CHARS) {
            self::hardWrap($text, '  ', str_repeat(' ', 25));
        } else {
            // Print the entire line if it's under 80 characters
            self::p($text);
        }
    }

    /**
     * Prints the options help message.
     */
    public static function printOptions()
    {
        self::p('Options:'."\n");

        foreach (self::getOptions() as $option => $description) {
            self::printOption($option, $description);
        }
    }

    /**
     * Prints the usage help message.
     */
    public static function printUsage()
    {
        self::p(self::getUsage());
    }
}
